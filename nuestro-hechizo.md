# Nuestro hechizo

Hay un hechizo escondido en cuatro direcciones, en cuatro servidoras, que puede ser reunido y lanzado con los comandos de invocación correctos.

Es un hechizo de confianza y empoderamiento, que aprendí de [Yeye Luisa Tesh](https://www.yeyeluisahteish.com/) en su libro Jambalaya<sup>1</sup>. Es uno de los primeros hechizos que aprendí, por lo que tiene un lugar especial en mi corazón, y estoy muy feliz de compartirlo.

Vamos a reunir los fragmentos en nuestra carpeta de `.hechizos`, luego invocar el hechizo lanzándolo todo a la vez en el orden correcto y leyéndolo en voz alta (preferiblemente viendo a un espejo).

Podés hacer este hechizo cuando sea que lo necesités, ejecutando estas invocaciones de nuevo.

Preparémonos moviéndonos a nuestra carpeta de hechizos...

Escribí:

```
$: cd ~/altar/.hechizos
```

---

<sup>1</sup>Yeye Luisa Tesh es una alta sacerdotisa yoruba, cuenta cuentos, autora, bailarina, y una linda ser cósmica. El libro que recomiendo es Jambalaya: The Natural Woman’s Book of Personal Charms and Practical Rituals. Es esta mezcla de memoria y manual inmensamente fácil de leer, y a mi legítimamente me cambió la vida. Está disponible en [Amazon](https://www.amazon.com/Jambalaya-Natural-Personal-Practical-Rituals/dp/0062508598/ref=asap_bc?ie=UTF8), y es probable que en [tu librería](https://www.lastwordbooks.org/).

---

< [Anterior: Hola curl](hola-curl.md) | [Siguiente: Reunir el hechizo](reunir-el-hechizo.md) >