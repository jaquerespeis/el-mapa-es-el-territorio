# Comandos como espíritus

Me he estado refiriendo a las palabras `cd`, `ls` y `pwd` como *comandos*, pero esta palabra no las describe por completo. Cada una no es solo una palabra, sino una *cosa* -- tiene su propia ubicación de residencia en tu computadora, su propia historia y autor+s. Es un programa que espera ciertas cosas al ser invocado y cuando las recibe hace lo que se le pide.

Pero sólo describirles como diminutos programas modulares tampoco es correcto. Esto no captura el linaje cultural, el sentido de memoria al verles, la identidad que tienen en nuestra mente cuando los escribimos, su naturaleza inescrutable cuando empezás a conocerles, ni tampoco lo maravilloso de que existan.

Para mi, espíritus es un mejor nombre para ellos: un conjunto de símbolos que crecen en vitalidad cada vez que les llamamos. Podés darles ofrendas a estos espíritus que van a influenciar lo que devuelven, pero a cada espíritu le gustan cosas diferentes, y funcionan mejor cuando les das lo que les gusta.

Por ejemplo, ambos `ls` y `cd` aman las rutas de archivos (e.g. `/home/jaqueer/Manifiestos/cyborg.txt`). Si les das una ruta existente, sólo listarán sus contenidos o se moverán a ella, respectivamente. Si no les das una, sólo listarán o se moverán a donde te encontrás en este momento.

Al saber lo que estos espíritus quieren, podemos construir invocaciones más específicas y poderosas. Para un pequeño ejemplo, escribí:

```
$: ls ~
```

`~` significa «directorio personal» y es por lo tanto una ruta válida. `ls` la recibe y hace lo que está destinado a hacer: devolver los archivos contenidos en `~`. Probá `ls ~/Documentos` y `ls ~/Fotos`.

De forma similar, en la línea de comandos no tenés que moverte por todo lado haciendo clic a través de carpetas una por una. Podés hacer `cd` hacia cualquier ruta y te llevará justo ahí. Si no le das ninguna ruta, entonces sólo te llevará al directorio personal.

Ahora vamos a casa. Escribí:

```
$: cd ~
```

< [Anterior: Cambiar de dirección](cambiar-de-direccion.md) | [Siguiente: Hacer un altar](hacer-un-altar.md) >