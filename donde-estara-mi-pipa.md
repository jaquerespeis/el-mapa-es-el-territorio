# ¿Y ahora dónde estará mi pipa?

Un símbolo mágico similar a `>>` es el poderoso `|`, también llamado «pipe» (una pipa o tubería, en inglés). Esta pipa permite redirigir la salida de un comando a la entrada de otro. Dicho de otra forma, permite invocar múltiples espíritus, cada uno transformando el flujo de energía que se le ofrece.

Por ejemplo, invoquemos los espíritus de la historia («history», en inglés).

---

## history

te mostrará una historia de tus comandos de bash, en orden.

*le gusta* sólo ser llamada

*devuelve* la historia con el número de comando, para que puedan ser llamados de nuevo

---

Digamos que queremos ver cada palabra a la que le hemos hecho eco durante esta sesión. Escribí:

```
$: history
```

Verás una lista de comandos, incluyendo algunos que se ven como:

```
57 echo "la internet es animista" >> bendicion

58 cat bendicion

59 mkdir .hechizos

60 ls

61 ls -a

62 ls -a ~

63 uname -a
```

La historia lista cada comando que hemos escrito en esta sesión, incluyendo nuestros comandos de eco. Ahora podemos desplazarnos por esta lista (haciendo «scroll» con el ratón) haciendo una nota mental de nuestros comandos de eco, pero esto sería difícil y no armoniza con nuestra intención.

En lugar de esto, podemos darle esta lista al espíritu `grep`

---

## grep

Busca un patrón en un flujo de texto, regresando las coincidencias.

*le gusta* flujos de texto y un patrón

*devuelve* un flujo de texto con todas las líneas que tienen ese patrón

*elevación* `--ignore-case` para ignorar diferencias de mayúsculas y minúsculas, `--after-context={n}` para incluir `n` líneas después de la coincidencia (e.g. `--after-context=4` incluirá la línea con el patrón y las 4 líneas siguientes)

---

Usando nuestra pipa podemos invocar `history` y luego pasar su ofrenda a nuestra invocación de `grep` para obtener exactamente lo que queremos.

En la terminal, escribí:

```
$: history | grep echo
```

Y ahora verás:

```
18 echo "ecooooo"

48 echo "la internet es animista" >> bendicion
```

Con pipas podemos unir invocaciones que aumenten en complejidad, para que sean más expresivas en bash, esta lengua adoptada.

Y, como veremos a continuación, el flujo de energía que pasamos a través de estas pipas puede venir de otros lugares fuera de nuestra computadora. Podemos atraer energía del otro lado del mundo para nuestras invocaciones.

---

< [Anterior: Hacer una carpeta de hechizos](hacer-una-carpeta-de-hechizos.md) | [Siguiente: Hola curl](hola-curl.md) >