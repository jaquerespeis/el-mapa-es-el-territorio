# Hacer eco de una bendición

Con este siguiente paso convertimos a un simple comando en parte de un hechizo. Con la línea de comandos, como en cualquier práctica de magia, estamos escuchando el flujo de energía y dirigiéndolo a nuestra voluntad. Los comandos, como todas las palabras, son flujos de energía.

Para mostrar esto vamos a crear una bendición y dejarla en nuestro altar.

Pensá en algo que querás recordar. Una palabra o frase bonita, algún mensaje para vos mism+ en este espacio.

Cuando estés lista, hacela un eco en tu computadora. Escribí:

```
$: echo "¡Bienvenid+ a mi altar!"
```

Ella devuelve:

```
¡Bienvenid+ a mi altar!
```

Como era de esperar, ella repite esta bendición debajo del prompt. Para ser más específic+s, `echo` tomó la bendición y la envió a la dirección que le pedimos. Como no le dimos ninguna dirección, hizo el eco en la pantalla de la terminal.

Tocá la flecha de arriba (para regresar al comando `echo` anterior) y agreguemos una dirección al final de estas palabras:

```
$: echo "¡Bienvenid+ a mi altar!" >> bendicion
```

Esta vez el mensaje no fue impreso abajo. El símbolo mágico `>>` dirige la salida de un comando y la escribe al final de cualquier archivo que nombrés<sup>1</sup>. Si ese archivo no existe, lo manifestará.

Para verlo, escribí:

```
$: ls
```

Y ahora verás:

```
bendicion
```

Tu altar tiene una bendición :)<sup>2</sup>.

Podés leer tu bendición con el confiable y flexible, comando `cat`.

---

## cat

concatenar muchos en uno

*le gusta* tantos archivos de texto como le des

*devuelve* la combinación de todos ellos en una gran cadena de texto

---

En la terminal, escribí:

```
$: cat bendicion
```

Lo que yo veo es:

```
¡Bienvenid+ a mi altar!
```

Si te nace, extendé tu bendición con otra frase bonita. Escribí:

```
$: echo "la internet es animista" >> bendicion
```

y luego:

```
$: cat bendicion
```

Lo que yo veo es:

```
¡Bienvenid+ a mi altar!

la internet es animista
```

Tu bonita nota crece con cada eco dirigido. ¡Esto me parece muy bueno!

---

<sup>1</sup>No confundir con su hermano más enérgico `>`, que *sobre escribirá* el archivo con tus palabras.

<sup>2</sup>Nunca supero esto, que la línea de comandos son palabras cargadas con palabras, energía en pausa con la forma de un nombre. Este sentimiento se pierde en la interfaz gráfica, donde todos los archivos se ven como papel para impresora. Cuando es solo texto, lo encuentro abrumadoramente bello. Eso es todo, ¡un poquito de formalidad autoral!

---

< [Anterior: Hacer un altar](hacer-un-altar.md) | [Siguiente: Hacer una carpeta de hechizos](hacer-una-carpeta-de-hechizos.md) >