# Hacer una carpeta de hechizos

Creemos un espacio en este altar para nuestros hechizos, y que sea un espacio *escondido*. Escribí:

```
$: mkdir .hechizos
```

(Notá el `.` antes de hechizos.)

Y luego:

```
$: ls
```

Sólo verás:

```
bendicion
```

Cualquier cosa que empiece con un punto estará escondida en la vista normal. Sin embargo, podemos pedirle a `ls` que nos muestre todo, incluso las cosas escondidas. Hacemos esto agregando un modificador a nuestra invocación. Escribí:

```
$: ls -a
```

Lo verás todo (a de «all», en inglés) <sup>1</sup>.

```
. .. .hechizos bendicion
```

Conforme aprendás este lenguaje y estés más familiarizad+ con estos comandos espíritus, la computadora se abrirá y te revelará más.

Probá:

```
$: ls --all ~
```

Hay varios archivos escondidos en tu directorio personal, invisibles hasta que sepás cómo preguntar por ellos.

Esta sorpresa continua, de algo presente pero invisible hasta que lo llamés, es una experiencia común en la línea de comandos<sup>2</sup>.

---

<sup>1</sup>Acerca de `.` y `..`: estos son enlaces simbólicos especiales que representan el directorio actual, y el directorio superior, respectivamente. Desde el directorio de altar podrías escribir `ls ..` y te listará todo lo que hay en el directorio personal. Estos enlaces simbólicos son especialmente útiles para moverse hacia arriba, como con `cd ..`.

<sup>2</sup>Algo divertido para probar es `uname --all`. Ahora te devolverá el nombre del sistema operativo y un montóóóón más. La computadora no quería abrirse a menos que se lo pidieras.

---

< [Anterior: Hacer eco de una bendición](hacer-eco-de-una-bendicion.md) | [Siguiente: ¿Y ahora dónde estará mi pipa?](donde-estara-mi-pipa.md) >