# Hacer un altar


Ya hemos hablado y nos hemos movido con la computadora, ahora *creemos*.

Dentro de tu directorio personal<sup>1</sup>, escribí:

```
$: mkdir altar
```

Como cd, mkdir hace su tarea de forma silenciosa. No verás ninguna confirmación, sólo un destello en la línea de comandos.

Sin embargo, al escribir:

```
$: ls
```

verás el altar en la lista junto a las carpetas en tu directorio personal:

```
altar Descargas Documentos Escritorio Fotos...
```


---

## mkdir

hacer un directorio

*le gusta* la ubicación de un directorio que no existe

*devuelve* ese directorio manifestado

---

Movámonos al altar:

```
$: cd altar
```

Luego confirmemos que estamos ahí:

```
$: pwd
```

Deberías ver algo como:

```
/home/VOS/altar
```

Veamos la lista de todo lo que hay aquí:

```
$: ls
```

Y esto no devuelve nada... Nos hemos manifestado a nosotr+s mism+s en un espacio vacío.

A continuación lo vamos a llenar con bendiciones y hechizos.

---

<Sup>1</sup>Podés revisar dónde estás con `pwd`. Deberías ver `/home/$VOS`, con $VOS siendo cualquiera que sea el nombre de usuaria que escogiste en esta computadora. Otra forma es sólo escribir `cd ~`.

---

< [Anterior: Comandos como espíritus](comandos-como-espiritus.md) | [Siguiente: Hacer eco de una bendición](hacer-eco-de-una-bendicion.md) >