# Lanzar el hechizo

Lanzamos el hechizo con `cat`, dándole todos los fragmentos en el orden que queremos:

```
$: cat inicio tierra agua fuego aire fin
```

Tu terminal se llenará con un hechizo. Leélo en vos alta para vos mism+, sentila sobre vos mism+. ¡Bien hecho! ¡Hiciste esto!

---

< [Anterior: Reunir el hechizo](reunir-el-hechizo.md) | [Siguiente: Adiós](adios.md) >
