# Hola curl

El sistema en tu computadora comenzó como un «mainframe» gigante para ser usado por muchas personas al mismo tiempo, y fue desarrollado al mismo tiempo que la internet. Y, tanto como una computadora *puede* desear, tu computadora desea conectarse con cosas fuera de ella, y que otr+s se conecten a ella. En el código fuente de la línea de comandos están las memorias de una comunidad mainframe, de una espora rica en información en una red de esporas.

Hay un montón de espíritus con el objetivo de estirar este mundo de texto más allá de tu computadora. Uno de mis favoritos es `curl`. Curl traerá datos de otra ubicación hasta tu computadora para unir estos flujos de energía, listos para ser dirigidos y transformados como lo querás.

---

## curl

descargar la web

*le gusta* una dirección web

*devuelve* todo el texto contenido en esa dirección

---

Podemos hacerle curl a los datos de sitios web de forma fácil con `curl` + *dirección web*.

En tu terminal, escribí:

```
$: curl --location http://solarpunk.cool
```

Te recibirá el código fuente de nuestra página de inicio, incluyendo todos los comentarios que no aparecen en la pantalla del navegador. <sup>1</sup>

Podés hacer esto con cualquier sitio. Probá con google.com y mirá el desastre de código javascript escondido detrás de su barra de búsqueda minimalista. Probá con johnclilly.com para que veás cómo se hacían los sitios web antes.

Hay algunos sitios que están diseñados para ser usados desde la terminal, un mundo subterráneo de páginas web. Mi favorita es wttr.in.

Escribí:

```
$: curl wttr.in
```

Verás una linda impresión del clima en tu ciudad. ¿Querés ver el clima en mi bella ciudad natal, Tumwater, Washington<sup>2</sup>? Escribí:

```
$: curl wttr.in/tumwater
```

Para el reporte avanzado del clima, probá:

```
$: curl v2.wttr.in
```

Y para ver una linda luna, probá:

```
$: curl wttr.in/Moon
```

Usaremos curl (y todo lo demás) para nuestro hechizo final.

---

<sup>1</sup>El `--location` es para que sigamos al sitio real. En este caso, redirige de http a https://

<sup>2</sup>Por su puesto podés escribir tu ciudad natal en lugar de la mía. No sabía cuál era y no quería ponerme en vergüenza delante tuyo.

---

< [Anterior: ¿Y ahora dónde estará mi pipa?](donde-estara-mi-pipa.md) | [Siguiente: Nuestro hechizo](nuestro-hechizo.md) >