# Crear el círculo

Antes de que empecemos a escribir, acomodémonos en el espacio y preparémonos.

La línea de comandos es calmada e introspectiva, y este hechizo lo será también. Si no podés estar en un espacio privado, si estás leyendo esta zine en un café bullicioso de una biblioteca de una gran ciudad, también podés hacer este hechizo, con audífonos.

Este hechizo es en su mayoría una visualización. Leelo todo primero, y luego movete a través de él con tus ojos cerrados.

Abrí tu terminal, tan grande como se pueda. Ahora, cerrá tus ojos. Sentí este espacio y sentí tu respiración ir más despacio de forma consciente. Concentrate en inhalar desde el estómago, luego exhalar suave y lento a través de la nariz.

Ahora visualizá el espacio a tu alrededor. Pensá en qué hay detrás de vos, frente a vos, a tu izquierda y derecha. ¿Qué hay arriba y abajo de vos? Sin abrir los ojos, enfocate en cada parte de la habitación en la que estás. ¿Estás sentad+? ¿Cómo es la silla? ¿De qué color son las paredes en este espacio? ¿Hay una ventana? ¿De qué color es la luz que entra por ella?

Enfocá tu atención en el espacio detrás de vos. Imaginalo con una claridad brillante y atención a los detalles. Sin abrir tus ojos, dirigí la mirada a lo más grande en esta dirección, imaginando cada detalle. Con una exhalación, mirá como este objeto se disuelve en una luz blanca. La forma aún está ahí, aún distinta entre todos los otros objetos en este espacio, pero su materia es pura luz. Visualizá todo lo demás en este espacio detrás de vos, exactamente como es, despacio transformándose en esta luz. Tomá cada objeto uno por uno, enfocate en él, y miralo disolverse en luz. Continuá haciendo esto hasta que todo el espacio detrás de vos sea brillante y sin forma.

Cuando estés list+, cambiá tu mirada interna hacia la izquierda. Visualizá cada detalle ahí con cristalina precisión. Uno por uno, enfocá y mirá cada objeto disolverse en esa pura energía blanca. Hacé esto hasta que todo a tu izquierda sea parte de la misma luz sin forma. Luego girá tu atención, y despacio, visualizá esto ocurriendo a todo lo que está a tu derecha. Imaginá todo en completo detalle, como si tus ojos estuvieran abiertos.

Cuando estés list+, visualizá todo frente a vos. Imaginá tu computadora, y la pantalla de la terminal abierta, y mirá como se disuelven en la misma luz. ¿Sobre qué está la computadora? Mirá eso también disolverse en la luz, junto con todo lo demás que lo rodea.

Dejá que la luz circundante crezca hasta que sea todo arriba y abajo de vos. Sentila irradiar a tu alrededor. Sentí la silla disolverse en la luz, sentí tus pies, tus piernas, tu pecho, tu cuerpo, tus brazos, tu cabeza disolverse en esta luz. Sin abrir los ojos, mirá alrededor de esta habitación. Mirá a cada objeto en donde lo dejaste, pero ahora indistinguible en este resplandor. Enfocá tu mirada, viendo con paciencia la pura luz blanca en su resplandor inmutable.

Ahora, escuchá a esta habitación en la que estás. Escuchás música. ¿De dónde viene esta música? Tal vez escuchás voces, los sonidos de afuera. ¿Dónde es afuera para este lugar en el que estás ahora? ¿Dónde vive el sonido?

Estás recordando las palabras de este hechizo, recordando cómo termina. ¿Dónde viven las palabras?

Liberate en esta habitación. Ninguna parte es distinta, pero aún se siente reconocible. ¿Qué es la presencia de la habitación cuando no tiene forma?

Quedate en este espacio hasta que la música termine. Luego, cuando estés list+, abrí tus ojos.

Cuando te sintás lista para crear el círculo, abrí este enlace, cerrá los ojos, y movete en él:

[Asleep on the Train, por Radical Face, del álbum Ghost](https://solarpunk.cool/zines/map-is-the-territory/aesthetic/asleep-on-a-train.mp3).

---

< [Anterior: Preparación](preparacion.md) | [Siguiente: Abrir la terminal](abrir-la-terminal.md) >
