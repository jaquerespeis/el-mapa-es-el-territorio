# Reunir el hechizo

El hechizo está escondido como fragmentos de cuatro líneas en el código fuente de un grupo de sitios web, señalados por el símbolo mágico del cometa: `~~*`.

## Inicio

Empecemos con un preludio de solarpunk.cool

En tu terminal, escribí:

```
$: curl --location solarpunk.cool/magic/inicio | grep "~~*" --after-context=2 | tail -2 >> inicio
```

(Esta ha sido nuestra invocación más compleja<sup>1</sup>, y debe ser escrita exactamente para que funcione de forma perfecta, entonces revisá lo que escribiste antes de presionar enter.)

Podemos revisar que funcionó con nuestro confiable `cat`:

```
$: cat inicio
```

Verás:

```
Tierra, agua, fuego y aire
dentro de mí todo está ahí
```

## Tierra

A continuación, tomamos la tierra de coolguy.website:

```
$: curl --location coolguy.website/tierra | grep "~~*" --after-context=2 | tail -2 >> tierra
```

## Agua

Tomamos el agua de angblev.com:

```
$: curl --location angblev.com/agua | grep "~~*" --after-context=2 | tail -2 >> agua
```

## Fuego

Tomamos el fuego de nuestra casa solarpunk.cool:

```
$: curl --location solarpunk.cool/magic/fuego | grep "~~*" --after-context=2 | tail -2 >> fuego
```

## Aire

Tomamos el aire de apileof.rocks:

```
$: curl --location apileof.rocks/aire | grep "~~*" --after-context=2 | tail -2 >> aire
```

## Fin

Terminamos el hechizo regresando a solarpunk.cool:

```
$: curl --location solarpunk.cool/magic/fin | grep "~~*" --after-context=2 | tail -2 >> fin
```

## Revisar que funcionó

Por último, escribí:

```
$: ls
```

deberías ver:

```
agua aire fin fuego inicio tierra
```

Ya tenemos las partes. Ahora podemos lanzar el hechizo.

---

<sup>1</sup>Aquí está el detalle de lo que estamos haciendo:

`curl --location solarpunk.cool/magic/inicio`: hace curl al código fuente desde esa URL, el `--location` significa que siga las redirecciones, lo que sólo asegura que recibamos el correcto en caso de que la servidora necesite redirigirnos (e.g., mover de http a https).

`| grep "~~*" --after-context=2`: luego pasamos ese código por una pipa hasta `grep`, que busca la línea «~~*». Agregando un `--after-context=2` significa que devuelve la línea con el cometa y las 2 líneas anteriores.

`| tail -2`: tomamos estas tres líneas de texto y las pasamos por una pipa hasta nuestro espíritu `tail` (cola, en inglés). No habíamos usado `tail` aún, pero lo que hace es tomar un flujo de texto y un número, N, y devuelve las últimas N líneas de ese texto. Aquí le estamos pidiendo las últimas 2 líneas, lo que significa que nos quedamos con las dos lineas del hechizo y dejamos que el cometa desaparezca.

`>> inicio`: por último, tomamos estas dos líneas y las condensamos en la palabra «inicio», guardado como un archivo en nuestra carpeta `.hechizos`.

¡Es un montón! ¡Pero también tan poquito! ¡Tanto con tan poco!

---

< [Anterior: Nuestro hechizo](nuestro-hechizo.md) | [Siguiente: Lanzar el hechizo](lanzar-el-hechizo.md) >
