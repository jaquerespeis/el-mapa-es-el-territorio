# Adiós

Es probable que ahora querás tomarte un descanso de la computadora, o te morís de ganas de revisar el correo. Antes, tomá un momento para poner ambos pies en el suelo, y poné tus manos sobre la mesa/escritorio/regazo sobre el que se encuentra tu computadora. Sentí todas las cosas que movimos hoy, todos los comandos e historias y términos técnicos y espíritus animistas, sentilos moverse dentro tuyo y salir hasta el suelo disipándose como agua en la tierra debajo de todo.

Gracias por pasar este rato con esta zine y con la terminal. Si avivó un deseo de conocer más sobre la línea de comandos y de las computadoras, aquí hay algunos recursos que recomiendo:

~~> [Tech Learning Collective](https://techlearningcollective.com/) (en inglés) es la mejor escuela, de manos a la obra, para aprender a trabajar con computadoras. Ofrecen talleres regulares y cursos largos, y tienen muy buenos [cursos básicos](https://techlearningcollective.com/foundations/) autodirigidos para hacer más cosas con la línea de comandos.

~~> Tu computadora ya tiene un libro de magia para la línea de comandos, que pasa desapercibido hasta que preguntás por él. Para cualquier espíritu, podés leer su «página de manual» con `man espíritu`, e.g., `man cat` o `man ls` o incluso `man man`. Cada espíritu tiene su propia página en este manual en crecimiento continuo. Aunque el lenguaje es un poco incómodo, la información es sólida, y el nivel de utilidad en la terminal realmente me inspira.

~~> Si no lo has hecho aún, vení a nuestro [club de magia solar punk](https://solarpunk.cool/magic/computer/club) (en inglés), que ocurre cada domingo a las 11 a.m. (hora de Nueva Zelanda, así que podría ser sábado en la tarde para quienes no son kiwis). Esta zine brotó de uno de estos días de club, ¡así que te da una idea general de lo que tratan! Son en línea en [tv.solarpunk.cool](https://tv.solarpunk.cool/).

~~> Acercate a tu JáquerEspeis tecno punk feminista queer anticapitalista decolonial local. Esta guía de magia fue traducida al español por l+s compas de Costa Rica que se juntan en el [bunqueer](https://bunqueer.jaquerespeis.org/).

Eso es todo. ¡Pura vida!

```
$: echo "Adiós, amig+"
```

---

< [Anterior: Lanzar el hechizo](lanzar-el-hechizo.md)
